package com.example.androidquiz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {

    private QuestionModel [] preguntas  = new QuestionModel[10];
    AlertDialog.Builder dialog;
    private TextView pregunta;
    private TextView numPreguntaTexto;
    private ProgressBar barraDeProgreso;
    int currentPregunta =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numPreguntaTexto=findViewById(R.id.prguntaActual);
         pregunta = findViewById(R.id.textV_pregunta);
         barraDeProgreso = findViewById(R.id.progressBar5);
        Button buttonFalse = findViewById(R.id.botton_false);
         buttonFalse.setOnClickListener(this);
        Button buttonTrue = findViewById(R.id.botton_true);
        buttonTrue.setOnClickListener(this);
        preguntas[0]=new QuestionModel(R.string.question1,true);
        preguntas[1]=new QuestionModel(R.string.question2,true);
        preguntas[2]=new QuestionModel(R.string.question3,false);
        preguntas[3]=new QuestionModel(R.string.question4,true);
        preguntas[4]=new QuestionModel(R.string.question5,false);
        preguntas[5]=new QuestionModel(R.string.question6,true);
        preguntas[6]=new QuestionModel(R.string.question7,true);
        preguntas[7]=new QuestionModel(R.string.question8,false);
        preguntas[8]=new QuestionModel(R.string.question9,false);
        preguntas[9]=new QuestionModel(R.string.question10,false);
        dialog = new AlertDialog.Builder(this);
    }


    public void showToastAndTest(Boolean  respuesta){
        if (preguntas[currentPregunta].isAcierto()==respuesta){
            Toast.makeText(this,"Correct",Toast.LENGTH_LONG).show();

        }else {
            Toast.makeText(this,"Incorrect",Toast.LENGTH_LONG).show();
        }
    }

    public void  actualizartextop(){
        pregunta.setText(preguntas[currentPregunta].getNumPregunta());
        String c = String.valueOf(currentPregunta+1);
        numPreguntaTexto.setText("Question "+c+"  of 10");

    }


    public void cambiarTextos(){
        currentPregunta++;
        if (currentPregunta%10==0){
            dialeg();
        }else {
            barraDeProgreso.incrementProgressBy(10);
          actualizartextop();
        }


    }


    @Override
    public void onClick(View v) {

            if (v.getId() == R.id.botton_true) {
                showToastAndTest(true);
                cambiarTextos();
            }else {
                showToastAndTest(false);
                cambiarTextos();
            }

    }
    public void dialeg(){
        dialog.setTitle("Congratulations, you  finished the quiz!");
        dialog.setMessage("What  do you want to   do  next");
        dialog.setNegativeButton("Finish", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
        dialog.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = getIntent();
                finish();
                startActivity(intent);;
            }
        });
        dialog.show();
    }
}