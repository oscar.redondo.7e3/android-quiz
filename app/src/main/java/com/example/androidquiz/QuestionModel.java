package com.example.androidquiz;

public class QuestionModel {
    int numPregunta;
    boolean acierto;

    public QuestionModel() {

    }


    public int getNumPregunta() {
        return numPregunta;
    }

    public void setNumPregunta(int numPregunta) {
        this.numPregunta = numPregunta;
    }

    public boolean isAcierto() {
        return acierto;
    }

    public void setAcierto(boolean acierto) {
        this.acierto = acierto;
    }
    public QuestionModel(int numPregunta, boolean acierto) {
        this.numPregunta = numPregunta;
        this.acierto = acierto;
    }


}
